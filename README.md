# coder-desafio-37

## capas:

el proyecto tiene las siguientes capas principales:

[routes]: paths y endpoints disponibles

[controllers]: orquesta los request y responses, también se ocupa de renderizar los html para este proyecto de tipo server side rendering

[services]: procesa y tiene la lógica de la funcionalidad, se conecta con los dao

[dao]: secontiene la conexión a la base de datos como los modelos, es el que se comunica con la base.

Hay otras carpetas complementarias en el mismo nivel que soportan estas capas base

[logs]: carpeta donde se guardan los logs generados con la estrategia de archivo

[auth]: contiene las estrategias de autenticación (passport, local)

[config]: expone la data del .env y configuraciones internas en un solo lugar

[resources]: repositorio de archivos y scripts complementarios del proyecto

[utils]: librerias de soporte

[views]: vistas y templates que utilizará el servidor

[layouts]: estructura de las vistas

[partials]: secciones de templates

##Estructura del proyecto

```
.
├── imgs
├── logs
├── performance
│   └── report
├── src
│   ├── auth
│   ├── config
│   ├── controllers
│   ├── dao
│   │   ├── db
│   │   └── models
│   ├── resources
│   ├── routes
│   ├── services
│   ├── utils
│   └── views
│   ├── layouts
│   └── partials
└── target
```

## Inicio del server

nodemon

19 directories

const productModel = require("../dao/models/product");
const logger = require("../utils/loggers");

const log = logger.getLogger("default");
const logFile = logger.getLogger("file");
const ERR_SAVE_MSG = "SERVICE Product: Not able to store product";
const ERR_GET_MSG = "SERVICE Product: Not able to get product";

module.exports = class {
  async addProduct(product) {
    try {
      const productCreated = await productModel.create(product);
      return productCreated;
    } catch (error) {
      log.error(ERR_SAVE_MSG);
      logFile.error(
        ERR_SAVE_MSG + " product: " + product.nombre + " error: " + error
      );
      return null;
    }
  }

  async getProduct(id) {
    try {
      return await productModel.findById(id);
    } catch (error) {
      log.error(ERR_GET_MSG);
      logFile.error(ERR_GET_MSG + " id: " + id + " error: " + error);
      return null;
    }
  }

  async getAllProducts() {
    return productModel.find();
  }

  async updateProduct(id, payload) {
    try {
      const productEdited = await productModel.findByIdAndUpdate(id, payload);
      return productEdited;
    } catch (error) {
      log.error(ERR_GET_MSG);
      logFile.error(ERR_GET_MSG + " id: " + id + " error: " + error);
      return null;
    }
  }

  async deleteProduct(id) {
    try {
      const productDeleted = await productModel.findByIdAndDelete(id);
      return productDeleted;
    } catch (error) {
      log.error(ERR_GET_MSG);
      logFile.error(ERR_GET_MSG + " id: " + id + " error: " + error);
      return null;
    }
  }
};

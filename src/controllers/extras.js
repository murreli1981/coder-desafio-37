const info = require("../utils/process-info");
const { fork } = require("child_process");
const getRandoms = require("../utils/child");

process.on("exit", (exit) => {
  console.log(`[${process.pid}] server exited with code ${exit}`);
});

exports.forceShutdown = async (req, res, next) => {
  process.exit();
};

exports.info = async (req, res, next) => {
  //console.log(`info added: ${info}`);
  res.json(info);
};

exports.randoms = async (req, res, next) => {
  const forked = fork("src/utils/child.js");

  const _default = 100000000;
  cant = req.query.cant || _default;

  // forked.send(cant || null);
  // forked.on("message", (freq) => {
  //   res.json(freq);
  // });

  frequency = getRandoms(cant);
  res.json(frequency);
};
